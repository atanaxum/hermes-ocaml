# Hermes, the state-of-the-art extension for oCaml

Add **Snippets** and **KeyBindings** for oCaml.

Permet d'avoir une **autocompletion** et les **raccourcis** assortis pour oCaml.

## Features

### KeyBindings (Raccourcis)

KeyBindings | Result | Result (french)
---------|----------|----------
 `Alt+N` | Function with a header block comment | Crée un corps de fonction avec le contrat et tests 
 `Alt+C` | Header block comment for a function | Crée un block de commentaires pour les contrats 
 `Alt+B` | Three empty tests | Crée un block de trois tests génériques
 `Alt+M, Alt+M` | Generic `match with` | Un `match with` générique
 `Alt+M, Alt+L` | Generic `match with` for lists | Un `match with` pour les listes
 `Alt+M, Alt+O` | Generic `match with` for option variables | Un `match with` pour les type option 
 `Alt+L, Alt+M` | Generic `List.map` | Un `List.map` generique
 `Alt+F, Alt+G` | Generic `List.fold_left` | Un `List.fold_left` generique
 `Alt+F, Alt+D` | Generic `List.fold_right` | Un `List.fold` right generique

### Snippets (Autocompletion)

Prefix | Result | Result (french)
---------|---------- |----------
 **Functions** | 
 `spec`, `new` | Function with a header block comment | Crée un corps de fonction avec le contrat et tests 
 `cc`, `contrat` | Header block comment for a function | Crée un block de commentaires pour les contrats 
 `btest`, `tests` | Three empty tests | Crée un block de trois tests génériques
 `af`, `aux` | Auxiliary recursive function | Crée une fonction auxiliaire recursive
 `fun`, `(fun)` | Anonymous function | Crée une fonction anonyme
 `test`, `let%test` | Generic test | Crée un test générique
 **Comparators** | 
 `if` | Generic `if...then...else` | Crée un `if...then...else` générique
 `match` | Generic `match with` | Un `match with` générique
 `ml`, `match_list` | Generic `match with` for lists | Un `match with` pour les listes
 `mo`, `match_option` | Generic `match with` for option variables | Un `match with` pour les type option 
 **Trees** |
 `tree`, `arbre_bin` | Generic binary tree with its implementation | Crée un type d'arbre binaire générique avec un arbre de test
 `tree-n`, `arbre_naire` | Generic n-branch tree with its implementation | Crée un type d'arbre n-aire générique avec un arbre de test et les getters
 **Lists** | 
 `left`, `fold_left` | Generic `List.fold_left` | Un `List.fold_left` generique
 `right`, `fold_right` | Generic `List.fold_right` | Un `List.fold_right` generique
 `map` | Generic `List.map` | Un `List.map` generique
 `iter` | Generic `List.iter` | Un `List.iter` generique

