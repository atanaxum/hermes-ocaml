# Change Log

All notable changes to the "hermes-ocaml" extension will be documented in this file.

## History

- 05/02/2021: Translation of the README into basic english ;)
- 04/03/2020: Possibilité d'utiliser les snippets avec menhir
- 27/02/2020: Correction des tabulations pour les operateurs de comparaison
- 26/02/2020: Amélioration des raccourcis